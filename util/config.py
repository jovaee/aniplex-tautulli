import json
import os
from argparse import ArgumentParser, BooleanOptionalAction, Namespace
from collections.abc import Callable
from dataclasses import dataclass
from typing import Any, Dict, Optional, Tuple


@dataclass
class AniplexConfig:
    anilist_username: str
    anilist_authkey: str

    auto_add: bool
    anidb_search: bool

    anidb_client_name: str | None = None
    anidb_client_version: str | None = None

    # Below are properties that returns a dictionary for the init params needed to initialize each item
    @property
    def anidb(self) -> dict:
        return {"name": self.anidb_client_name, "version": self.anidb_client_version}

    @property
    def anilist(self) -> dict:
        return {"authkey": self.anilist_authkey}


def parse_arguments() -> tuple[AniplexConfig, str]:
    """
    Parse arguments from the CLI, environment variables and config.json
    """

    ARGUMENTS = {
        "--filepath": {"type": str, "help": "Path to the file to process (Required)"},
        "--anilist-username": {"type": str, "help": "Anilist username (Required)"},
        "--anilist-authkey": {
            "type": str,
            "help": "Anilist authentication key (Required)",
        },
        "--anidb-client-name": {
            "type": str,
            "help": "AniDB client name. Required if --auto-add enabled",
        },
        "--anidb-client-version": {
            "type": int,
            "help": "AniDB client version. Required if --auto-add enabled",
        },
    }
    REQUIRED_ARGS = ("filepath", "anilist_username", "anilist_authkey")

    def load_value_into_namespace(
        key: str, value: Any, type: Callable, namespace: Namespace
    ):
        """
        Set `key` to value `value` in `namespace`. `value` has to be of type `type`.
        """
        nonlocal parser
        try:
            type(value)
        except ValueError:
            parser.error(f"--{key}: invalid {type.__name__} value: {value}")
        else:
            setattr(namespace, key.replace("-", "_"), type(value))

    def load_file_args(namespace: Namespace) -> None:
        """
        Load arguments values from config.json if it exists.
        """
        if os.path.isfile("config.json"):
            with open("config.json", "r", encoding="utf-8") as file:
                config_contents = json.load(file)

                for name, settings in ARGUMENTS.items():
                    if name[2:] in config_contents:
                        load_value_into_namespace(
                            name[2:],
                            config_contents[name[2:]],
                            settings["type"],
                            namespace,
                        )

    def load_env_args(namespace: Namespace) -> None:
        """
        Load arguments from environment variables if they exist
        """
        for name, settings in ARGUMENTS.items():
            formatted_key = name[2:].replace("-", "_").upper()
            if formatted_key in os.environ:
                load_value_into_namespace(
                    name[2:], os.environ.get(formatted_key), settings["type"], namespace
                )

    namespace = Namespace()
    parser = ArgumentParser(description="Aniplex")

    # Add trigger arguments
    parser.add_argument(
        "--auto-add",
        default=False,
        action=BooleanOptionalAction,
        help="Automatically adding missing entries to your Anilist",
    )
    parser.add_argument(
        "--anidb-search",
        default=False,
        action=BooleanOptionalAction,
        help="Use AniDB for fetching data to match anime in Anilist",
    )

    # Add value arguments
    for name, settings in ARGUMENTS.items():
        parser.add_argument(name, **settings)

    # The order dictates which one takes priority
    load_file_args(namespace)
    load_env_args(namespace)

    args = parser.parse_args(namespace=namespace)

    # Make sure required arguments were provided via the CLI or config file
    for required_arg in REQUIRED_ARGS:
        if not getattr(args, required_arg, None):
            parser.error(f"--{required_arg.replace('_', '-')} is required")

    # Some additional required checks
    if args.auto_add:
        if not args.anidb_client_name:
            parser.error("--anidb-client-name is required when --auto-add enabled")
        if not args.anidb_client_version:
            parser.error("--anidb-client-version is required when --auto-add enabled")

    args_dict = args.__dict__
    filepath = args_dict.pop("filepath")

    return AniplexConfig(**args_dict), filepath
