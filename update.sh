set -e

echo "#############################"
echo "#      AniPlex Updater      #"
echo "#############################"

echo Downloading newest version...
wget https://bitbucket.org/jovaee/aniplex-tautulli/get/master.zip -q

echo Applying update...
unzip -q master.zip
cp -r jovaee-aniplex-tautulli*/* .

echo Cleaning up...
rm master.zip
rm -rf jovaee-aniplex-tautulli*

echo Done
