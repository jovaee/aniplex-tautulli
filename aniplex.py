import logging
import os
import sys
from datetime import datetime

from clients import AniDBClient, AnilistClient, ArmServerClient
from exceptions import AnilistNotFound, ArmServiceNotFound
from model import AniDBMedia, AnilistMedia, AnilistSearchMedia, WatchStatus
from util.config import AniplexConfig, parse_arguments
from util.io import load_mappings, read_anidb_cached_data, write_anidb_cached_data
from util.util import extract_anidb_id, extract_episode, sanitize_title

LOG_FORMAT = "[%(name)s] %(asctime)s - %(levelname)s - %(message)s"

CUR_DIR = os.path.abspath(os.path.dirname(__file__))
CONFIG_FILE_NAME = "config.json"
MAPPINGS_FILE_NAME = "mappings.txt"

# Setup the logger
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.INFO)
handler.setFormatter(logging.Formatter(LOG_FORMAT))

logging.basicConfig(
    filename=f"{CUR_DIR}/aniplex.log",
    filemode="a",
    format=LOG_FORMAT,
    level=logging.INFO,
)
logger = logging.getLogger("AniPlex")
logger.addHandler(handler)


class Aniplex:

    def __init__(self, config: AniplexConfig):
        self.config = config
        self.configure()
        self.load_mappings()

    def configure(self) -> None:
        self.anidb = AniDBClient(**self.config.anidb)
        self.anilist = AnilistClient(**self.config.anilist)
        self.arm_server = ArmServerClient()

    def load_mappings(self) -> None:
        """
        Load user defined mappings
        """
        mappings: dict[int, int] = {}
        if os.path.exists(os.path.join(CUR_DIR, MAPPINGS_FILE_NAME)):
            mappings, invalid_mappings = load_mappings(
                os.path.join(CUR_DIR, MAPPINGS_FILE_NAME)
            )

            # Output any invalid mappings
            for line in invalid_mappings:
                logger.warning("Invalid mapping: %s", line)

        self.mappings = mappings

    def search_anidb(self, anidb_id: int) -> AniDBMedia | None:
        """
        Get AniDB data with the given ID. Reads from cached file if it's available
        """
        # Use anime cached data if possible otherwise fetch it and cache it
        anidb_data = read_anidb_cached_data(anidb_id, CUR_DIR)
        if not anidb_data:
            anidb_data = self.anidb.get_anidb_entry_raw(anidb_id)
            # TODO: Make sure it isn't a ban response :p
            write_anidb_cached_data(anidb_id, anidb_data, CUR_DIR)
        else:
            logger.info("Using cached data for AniDB (%s) data", anidb_id)

        return self.anidb.raw_data_to_model(anidb_data)

    def search_arm_server(self, anidb_id: int) -> int | None:
        """
        Search for a mapping on arm-server
        """
        logger.info("Searching arm-server for AniDB ID %s", anidb_id)
        anilist_id = self.arm_server.get_anilist_id_from_anidb_id(anidb_id)
        if anilist_id:
            logger.info(
                "Found AniDB (%s) to Anilist (%s) ID mapping on arm-server",
                anidb_id,
                anilist_id,
            )

        return anilist_id

    def search_anilist(
        self,
        title: str | None = None,
        air_date: datetime | None = None,
        anilist_id: int | None = None,
    ) -> AnilistSearchMedia | None:
        """
        Search Anilist for a matching item given filters
        """
        filters = {
            "title_native": sanitize_title(title) if title else None,
            "air_date": air_date,
            "id": anilist_id,
        }

        logger.info("Searching AniList with filters %s", filters)
        return self.anilist.search_media(**filters)

    def find_anilist_media(self, anidb_id: int) -> AnilistSearchMedia | None:
        """
        Given an AniDB ID, find the corresponding Anilist entry
        """
        logger.info("Looking for AniDB ID %s", anidb_id)

        searched_media: AnilistSearchMedia | None = None

        try:
            mapping_anilist_id = self.mappings.get(anidb_id)

            try:
                if mapping_anilist_id:
                    # Use the mapping override
                    logger.info(
                        "Searching AniList for ID %s via custom mapping",
                        mapping_anilist_id,
                    )
                    searched_media = self.search_anilist(anilist_id=mapping_anilist_id)
                else:
                    # Search for the Anilist item using data from other services
                    if self.config.anidb_search:
                        # Search Anilist using AniDB data
                        anidb_data = self.search_anidb(anidb_id)
                        logger.info("Found AniDB media %s", anidb_data)

                        if anidb_data:
                            searched_media = self.search_anilist(
                                title=anidb_data.title_native,
                                air_date=anidb_data.air_date,
                            )
            except AnilistNotFound:
                logger.info(
                    "Item not found on Anilist with conventional filters. Falling back to arm-server"
                )

            # Either AniDB search is disabled or no match was found using Title and Air Date, so try arm-server
            if not searched_media:
                anilist_id = self.search_arm_server(anidb_id)
                searched_media = self.search_anilist(anilist_id=anilist_id)
        except ArmServiceNotFound:
            logger.error("Could not find Anilist media item")
        except Exception:
            logger.exception("Could not find Anilist media item due to error")
        finally:
            return searched_media

    def find_media_in_user_list(
        self, searched_media: AnilistSearchMedia
    ) -> AnilistMedia | None:
        """
        Attempt to find the same media item in the user's personal list
        """
        user_list = self.anilist.get_user_list(self.config.anilist_username)
        for item in user_list:
            if item.media_id == searched_media.id:
                return item

        return None

    def add_media_to_user_list(
        self, searched_media: AnilistSearchMedia, episode: int
    ) -> None:
        """
        Add the given media item to the user's personal list with progress set to `episode`
        """
        logger.info("Did not find matching item in user's list")
        self.anilist.upsert_user_media_entry(
            searched_media.id, episode, set_as_watching=True, started_at=datetime.now()
        )

        logger.info(
            "Added %s to user's list and updated episode %s/%s",
            searched_media.id,
            episode,
            searched_media.episodes or "-",
        )

    def update_user_progress(self, media: AnilistMedia, episode: int) -> None:
        """
        Update the progress of the of a media item in the user's list
        """
        if media.status == WatchStatus.COMPLETED:
            logger.info(
                "No need to update user list as item is already completed %s/%s",
                media.progress,
                media.episodes,
            )
            return
        if media.progress >= episode:
            # The current watched episode is smaller than what is on Anilist so there is no need for an update
            logger.info(
                "Anilist progress (%s) is larger or equal to current episode (%s)",
                media.progress,
                episode,
            )
            return

        try:
            # Update Anilist progress so that it matches the current episode
            # Only set as watching when we watched the first episode and there are more than one episode in total
            # If Anilist doesn't know the number of episodes yet, denoted by item.episodes == None, then we assume there
            # are more than one episode and an item will not be marked as Complete after the first episode
            set_as_watching = media.progress == 0 and (
                media.episodes > 1 if media.episodes is not None else True
            )
            started_at = datetime.now() if media.progress == 0 else None
            completed_at = (
                datetime.now()
                if media.episodes is not None and episode == media.episodes
                else None
            )
            self.anilist.upsert_user_media_entry(
                media.media_id,
                episode,
                set_as_watching=set_as_watching,
                started_at=started_at,
                completed_at=completed_at,
            )

            args = [
                media.title_media,
                media.episode_progress,
                episode,
                media.episodes or "-",
            ]
            if episode == media.episodes:
                # The last episode was just watched
                args.append(media.site_url)

            logger.info(
                "Updated %s to episode %s/%s",
                media.media_id,
                episode,
                media.episodes or "-",
            )
        except Exception:
            logger.exception("Could not update Anilist episode due to error")

    def run(self, filepath: str) -> None:
        """
        Run Aniplex on the given filepath
        """
        anidb_id = extract_anidb_id(filepath)
        if anidb_id is None:
            logger.info("No AniDB ID was found in filepath")
            return

        episode = extract_episode(filepath)
        if not episode:
            logger.info(
                'Filename "%s" did not match required format to extract episode number',
                os.path.basename(filepath),
            )
            return

        searched_media = self.find_anilist_media(anidb_id)
        if not searched_media:
            return

        logger.info("Found Anilist media %s", searched_media)

        user_list_media = self.find_media_in_user_list(searched_media)
        if not user_list_media:
            if self.config.auto_add:
                # The item was not found in the user's list, so add it
                self.add_media_to_user_list(searched_media, episode)

            return

        logger.info("Found matching item in user's list %s", user_list_media)
        self.update_user_progress(user_list_media, episode)


if __name__ == "__main__":
    if not os.path.exists(os.path.join(CUR_DIR, "cache")):
        logger.info("Cache directory not found, creating...")
        os.mkdir(os.path.join(CUR_DIR, "cache"))

    config, filepath = parse_arguments()
    logger.info('Working with file "%s"', filepath)

    Aniplex(config).run(filepath)
