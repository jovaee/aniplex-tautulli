from dataclasses import dataclass
from datetime import datetime
from enum import Enum


class WatchStatus(Enum):
    COMPLETED = "Completed"
    PLANNING = "Planning"
    CURRENT = "Current"
    PAUSED = "Paused"
    DROPPED = "Dropped"


class AirStatus(Enum):
    FINISHED = "Finished"
    RELEASING = "Releasing"
    NOT_YET_RELEASED = "Not_yet_released"
    CANCELLED = "Cancelled"
    HIATUS = "Hiatus"


@dataclass
class AnilistBase:

    @property
    def episode_progress(self) -> str:
        # Default self.episodes to - if the anime doesn't have a set number of episode yet
        return "{}/{}".format(self.progress or "-", self.episodes or "-")

    @property
    def title_media(self) -> str:
        return r"{} [{}]".format(self.title_romaji, self.media_id)


@dataclass
class AnilistMedia(AnilistBase):

    media_id: int  # Unique ID
    progress: int  # How many episodes user has watched
    score: float  # User's rating
    title_native: str  # Title in kanji etc
    status: WatchStatus  # Media user status
    site_url: str  # URL

    episodes: int | None = None  # Total number of episodes
    title_romaji: str | None = None  # Title in roman characters
    air_date: datetime | None = None  # When the first episode aired
    end_date: datetime | None = None  # When the last episoded aired
    start_date: datetime | None = None  # When the user watched the first episode
    complete_date: datetime | None = None  # When the user watched the last episode
    mean_score: float | None = None  # Mean user rating

    def __post_init__(self):
        self.status = WatchStatus(self.status.capitalize()) if self.status else None


@dataclass
class AnilistSearchMedia(AnilistBase):

    id: int  # Unique ID
    status: AirStatus  # Media user status
    episodes: int  # Total number of episodes
    title_native: str  # Title in kanji etc
    site_url: str  # URL

    title_romaji: str | None = None  # Title in roman characters
    air_date: datetime | None = None  # When the first episode aired
    end_date: datetime | None = None  # When the last episoded aired
    mean_score: float | None = None  # Mean user rating

    def __post_init__(self):
        self.status = AirStatus(self.status.capitalize()) if self.status else None


@dataclass
class AniDBMedia:

    media_id: int  # Unique ID
    episodes: int | None = None  # Total number of episodes
    title_native: str | None = None  # Title in kanji etc
    title_romaji: str | None = None  # Title in roman characters
    air_date: datetime | None = None  # Date first episode aired
    end_date: datetime | None = None  # Date last episode aired
