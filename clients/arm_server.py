import requests

from exceptions import ArmServiceNotFound

URL = "https://relations.yuna.moe/api/ids"


class ArmServerClient:

    @staticmethod
    def get_anilist_id_from_anidb_id(anidb_id: int) -> int | None:
        """
        Get AniDB -> Anilist mapping using the arm-server mapping library
        """
        # The response code will always be 200 (OK). If an entry is not found null is returned instead.
        response = requests.post(
            URL, headers={"Content-Type": "application/json"}, json={"anidb": anidb_id}
        ).json()
        if not response:
            raise ArmServiceNotFound

        anilist_id = [v for k, v in response.items() if k == "anilist"]
        if anilist_id:
            return anilist_id[0]

        return None
