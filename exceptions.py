class NotFound(Exception):
    pass


class AnilistNotFound(NotFound):
    pass


class AniDBNoitFound(NotFound):
    pass


class ArmServiceNotFound(NotFound):
    pass


class AnilistApiError(Exception):

    def __init__(self, msg: str, status_code: int):
        self.msg = msg
        self.status_code = status_code
