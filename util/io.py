import os
from time import time


def load_mappings(path: str) -> tuple[dict[int, int], list[str]]:
    """
    Read the override mappings file
    """
    mappings = {}
    invalid_mappings = []

    with open(path, "r", encoding="utf-8") as file:
        for line in file:
            parts = line.split("#")  # Remove comments
            if parts:
                mapping = parts[0].split(":")  # Split mapping
                if len(mapping) == 2:
                    anidb, anilist = mapping[0].strip(), mapping[1].strip()
                    mappings[int(anidb)] = int(anilist)
                else:
                    invalid_mappings.append(line)

    return mappings, invalid_mappings


def read_anidb_cached_data(
    anidb_id: int, pwd: str, max_cache_age: int = 60 * 60 * 24
) -> str | None:
    """
    Get AniDB anime data from cached file if it's not considered expired
    """
    path = os.path.join(pwd, "cache", f"{anidb_id}.xml")
    if os.path.exists(path) and time() - os.path.getmtime(path) < max_cache_age:
        with open(path, encoding="utf-8") as xml_file:
            return xml_file.read()

    return None


def write_anidb_cached_data(anidb_id: int, xml_data: bytes, pwd: str) -> None:
    """
    Save AniDB anime data to cache file
    """
    path = os.path.join(pwd, "cache", f"{anidb_id}.xml")
    with open(path, "wb") as xml_file:
        xml_file.write(xml_data)
