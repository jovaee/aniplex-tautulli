import os
import re

# https://regex101.com/r/0NqMzO/2
ANIDB_REGEX = r".*?\[anidb\-(?P<id>\d+)\].*"
EPISODE_REGEX = r"\[(?P<subber>.+)\]\s(?P<title>.+?)(?:\s-\s(?P<episode>[0-9]{1,4}(?:\.5|[A-Z])?))?\s\[(?P<resolution>\d+p)\].(?P<extension>.+)"
YEAR_REGEX = r"(.*?)\((?P<year>\d{4})\)"


def extract_anidb_id(filepath: str) -> int | None:
    """
    Extract the AniDB ID from filepath if it is present
    """
    match = re.match(ANIDB_REGEX, filepath)
    if match:
        return int(match.group("id"))

    return None


def extract_episode(filepath: str) -> int | None:
    """
    Extract episode number from filepath
    """
    match = re.match(EPISODE_REGEX, os.path.basename(filepath))
    if match:
        if match.group("episode") is None:
            return 1

        return int(match.group("episode"))

    return None


def sanitize_title(title: str) -> str:
    """
    Sanitize the title a bit to try and improve correct matches
    """
    title = title.strip()

    # Remove release year from title so something like "One Punch Man (2019)"
    match = re.match(YEAR_REGEX, title)
    if match:
        title = title if match.group("year") is None else match.group(1).strip()

    return title
