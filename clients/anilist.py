from datetime import datetime
from typing import Any

import requests

from exceptions import AnilistApiError, AnilistNotFound
from model import AnilistMedia, AnilistSearchMedia

ANILIST_GRAPHQL_URL = "https://graphql.anilist.co/"
ANILIST_OAUTH_URL = "https://anilist.co/api/v2/oauth/token"


class AnilistClient:

    def __init__(self, authkey: str):
        self.authkey = authkey

    def _make_request(self, query: str, variables: dict[str, Any]) -> dict[str, Any]:
        response = requests.post(
            ANILIST_GRAPHQL_URL,
            headers={
                "Authorization": f"Bearer {self.authkey}",
                "Content-Type": "application/json",
                "Accept": "application/json",
            },
            json={"query": query, "variables": variables},
        )
        if response.status_code == 404:
            raise AnilistNotFound
        elif response.status_code != 200:
            raise AnilistApiError(response.json(), response.status_code)
        else:
            return response.json()

    @staticmethod
    def date_to_datetime(date: dict[str, int]) -> datetime | None:
        return (
            datetime(year=date["year"], month=date["month"], day=date["day"])
            if date["day"]
            else None
        )

    @staticmethod
    def json_to_model(entry: dict) -> AnilistMedia:
        start_date = AnilistClient.date_to_datetime(entry["startedAt"])
        complete_date = AnilistClient.date_to_datetime(entry["completedAt"])
        air_date = AnilistClient.date_to_datetime(entry["media"]["startDate"])
        end_date = AnilistClient.date_to_datetime(entry["media"]["endDate"])

        return AnilistMedia(
            media_id=entry["mediaId"],
            progress=entry["progress"],
            score=entry["score"],
            episodes=entry["media"]["episodes"],
            title_romaji=entry["media"]["title"]["romaji"],
            title_native=entry["media"]["title"]["native"],
            start_date=start_date,
            complete_date=complete_date,
            status=entry["status"],
            mean_score=entry["media"]["meanScore"],
            site_url=entry["media"]["siteUrl"],
            air_date=air_date,
            end_date=end_date,
        )

    @staticmethod
    def json_to_search_model(entry: dict) -> AnilistSearchMedia:
        air_date = AnilistClient.date_to_datetime(entry["startDate"])
        end_date = AnilistClient.date_to_datetime(entry["endDate"])

        return AnilistSearchMedia(
            id=entry["id"],
            episodes=entry["episodes"],
            title_romaji=entry["title"]["romaji"],
            title_native=entry["title"]["native"],
            air_date=air_date,
            end_date=end_date,
            status=entry["status"],
            mean_score=entry["meanScore"],
            site_url=entry["siteUrl"],
        )

    def search_media(
        self,
        title_native: str | None = None,
        season_year: int | None = None,
        air_date: datetime | None = None,
        id: int | None = None,
    ) -> AnilistSearchMedia:
        """
        Search Anilist for Anime with a given native name.
        """
        variables = {}
        if title_native:
            variables["search"] = title_native
        if season_year:
            variables["seasonYear"] = season_year
        if air_date:
            variables["startDate"] = air_date.strftime("%Y%m%d")
        if id:
            variables["id"] = id

        if not variables:
            raise ValueError("No filters specified")

        query = """
        query ($id: Int, $search: String, $seasonYear: Int, $startDate: FuzzyDateInt) {
            Media (id: $id, search: $search, seasonYear: $seasonYear, startDate: $startDate) {
                id
                status
                episodes
                title {
                    romaji
                    native
                }
                startDate {
                    year
                    month
                    day
                }
                endDate {
                    year
                    month
                    day
                }
                meanScore
                siteUrl
            }
        }
        """
        response = self._make_request(query, variables)
        media = response["data"]["Media"]
        return self.json_to_search_model(media)

    def get_user_list(self, username: str) -> list[AnilistMedia]:
        """
        Get list of all user's anime
        """
        variables = {"username": username}
        query = """
        query ($username: String) {
            MediaListCollection (userName: $username, type: ANIME) {
                lists {
                    entries {
                        id
                        mediaId
                        progress
                        status
                        score
                        startedAt {
                            year
                            month
                            day
                        }
                        completedAt {
                            year
                            month
                            day
                        }
                        media {
                            siteUrl
                            meanScore
                            episodes
                            title {
                                romaji
                                native
                            }
                            startDate {
                                year
                                month
                                day
                            }
                            endDate {
                                year
                                month
                                day
                            }
                        }
                    }
                }
            }
        }
        """
        response = self._make_request(query, variables)
        return [
            self.json_to_model(media)
            for list_ in response["data"]["MediaListCollection"]["lists"]
            for media in list_["entries"]
        ]

    def upsert_user_media_entry(
        self,
        media_id: int,
        progress: int,
        set_as_watching: bool = False,
        started_at: datetime | None = None,
        completed_at: datetime | None = None,
    ) -> AnilistMedia:
        """
        Add/Update user Anilist media item
        """
        variables: dict[str, int | str | dict[str, int]] = {
            "mediaId": media_id,
            "progress": progress,
        }
        if set_as_watching:
            variables["status"] = "CURRENT"
        if started_at:
            variables["startedAt"] = {
                "year": started_at.year,
                "month": started_at.month,
                "day": started_at.day,
            }
        if completed_at:
            variables["completedAt"] = {
                "year": completed_at.year,
                "month": completed_at.month,
                "day": completed_at.day,
            }

        query = """
        mutation ($mediaId: Int, $progress: Int, $status: MediaListStatus, $startedAt: FuzzyDateInput, $completedAt: FuzzyDateInput) {
            SaveMediaListEntry(mediaId: $mediaId, progress: $progress, status: $status, startedAt: $startedAt, completedAt: $completedAt) {
                id
                mediaId
                progress
                status
                score
                startedAt {
                    year
                    month
                    day
                }
                completedAt {
                    year
                    month
                    day
                }
                media {
                    siteUrl
                    meanScore
                    episodes
                    title {
                        romaji
                        native
                    }
                    startDate {
                        year
                        month
                        day
                    }
                    endDate {
                        year
                        month
                        day
                    }
                }
            }
        }
        """
        response = self._make_request(query, variables)
        media = response["data"]["SaveMediaListEntry"]
        return self.json_to_model(media)
