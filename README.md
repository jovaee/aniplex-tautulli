# AniPlex-Tautulli

AniPlex-Tautulli is script that syncs your Plex library progress with Anilist. Every time an item is marked as watched Tautulli is used to trigger AniPlex-Tautulli and it attempts to update your Anilist anime. It updates your episode progress, status, start date and finish date. It requires anime to already be present in a user's anime Anilist library for any updating to happen.

**Please note:**
AniPlex-Tautulli is heavily reliant on the folder/file structure used by my [anime manager](https://bitbucket.org/jovaee/anime-scripts/src/master/manager/). If this structure is not followed things will not work.  It's advised to also use it for best results.

## Setup

### Requirements

* [Plex](https://www.plex.tv/)
* [Tautulli](https://tautulli.com/)
* [AniDB Client](https://anidb.net/perl-bin/animedb.pl?show=client)

### Config File

Create a `config.json` file with the following structure and input the correct values.

```json
{
    "username": "AnilistUsername",
    "authkey": "IPromiseIDidNotDoIt",
    "client_name": "AniDBClient",
    "client_version": 1
}
```

### Anilist Authentication

An authorization token is required in order to update a user's Anilist library.
This token can be obtained from [here](https://anilist.co/api/v2/oauth/authorize?client_id=3447&response_type=token). Login if required and click the Authorize button.

Copy the key from the textarea and past it in the `config.json` file in the `authkey` field.

### Setting up Tautulli

It's required to setup a trigger in Tautulli that will execute AniPlex-Tautulli to do the syncing. To do this follow the following steps

 1. In `Settings` add a new `Notification Agent` of type `Script`
 2. Click on the newly created `Notification Agent`
    1. Under `Configuration` tab
        * Enter the path to where you cloned the repo in the `Script Folder` textbox
        * Select `aniplex.py` from the `Script File` dropdown. If it's not showing the path entered in `Script Folder` is incorrect
    2. Under the `Triggers` tab
        * Click on `Watched` so that the script will trigger when an item has been marked as watched in Plex
    3. Under the `Arguments` tab
        * Click on `Watched` and add `--filepath {file}` to `Script Arguments`
 3. Click `Save`

## Custom Mappings

For the most part AniDB to Anilist mappings are usually correct. Using a combination of `Title` and `Start Date` to search for a particular anime on Anilist usually gives the correct Anilist item back. The `Start Date` is also used since raw string searches are only so reliable and with this much better accuracy can be achieved. However sometimes there are subtle differences between AniDB and Anilist `Start Date` values. When this happens accuracy falls out the window and incorrect mappings will happen. Custom mappings allows the user to control AniDB to Anilist ID mappings without AniPlex having to do any searching.

Simply create a file `mappings.txt` and put mappings on a line by line basis in the format `AniDB_ID:Anilist_ID`. A comment can be added after a `#` character if desired: `mappings.txt` example:

```text
15484:117193 # Boku no Hero Academia 5th Season
14416:104276 # Boku no Hero Academia 4th Season
```

## TODO

* Improve anime status updating to take into account Anilist entry status
