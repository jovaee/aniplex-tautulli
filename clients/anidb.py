from datetime import datetime
from xml.etree import ElementTree

import requests

from model import AniDBMedia

XML_LANG = "{http://www.w3.org/XML/1998/namespace}lang"

LANG_JA = "ja"
LANG_XJAT = "x-jat"

TITLE_TYPE_MAIN = "main"
TITLE_TYPE_OFFICIAL = "official"

API_URL = "http://api.anidb.net:9001/httpapi?request=anime&client={client}&clientver={clientver}&protover=1&aid={aid}"


class AniDBClient:

    def __init__(self, name: str, version: int):
        self.name = name
        self.version = version

    @staticmethod
    def raw_data_to_model(data: str) -> AniDBMedia:
        """
        Convert AniDB XML data to AniDB model
        """
        xml_doc = ElementTree.fromstring(data)

        adbid = xml_doc.attrib["id"]
        episode_count = xml_doc.find("episodecount").text

        air_date = xml_doc.find("startdate")
        end_date = xml_doc.find("enddate")
        if air_date is not None:
            air_date = datetime.strptime(air_date.text, "%Y-%m-%d")
        if end_date is not None:
            end_date = datetime.strptime(end_date.text, "%Y-%m-%d")

        title_native = [
            title.text
            for title in xml_doc.findall("*/title")
            if title.attrib[XML_LANG] == LANG_JA
            and title.attrib["type"] == TITLE_TYPE_OFFICIAL
        ][0]
        title_romaji = [
            title.text
            for title in xml_doc.findall("*/title")
            if title.attrib[XML_LANG] == LANG_XJAT
            and title.attrib["type"] == TITLE_TYPE_MAIN
        ][0]

        return AniDBMedia(
            media_id=adbid,
            episodes=episode_count,
            title_romaji=title_romaji,
            title_native=title_native,
            air_date=air_date,
            end_date=end_date,
        )

    def get_anidb_entry_raw(self, aid: int) -> bytes:
        """
        Get AniDB data from API request in XML format
        """
        url = API_URL.format(
            **{"client": self.name, "clientver": self.version, "aid": aid}
        )

        response = requests.get(url=url)
        return response.content

    def get_anidb_entry(self, aid: int) -> AniDBMedia:
        """
        Get AniDB data from API request in model format
        """
        data = self.get_anidb_entry_raw(aid)
        return AniDBClient.raw_data_to_model(data)
